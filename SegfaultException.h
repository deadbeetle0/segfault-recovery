#ifndef SEGFAULTEXCEPTION_H
#define SEGFAULTEXCEPTION_H

#include <string>
#include <sstream>
#include <exception>

class SegfaultException : public std::exception {
  public:
    template <typename T>
    SegfaultException(T *p) : m_ptr(p), m_ptr_size(sizeof(T)) {
      std::stringstream ss;
      ss << "A segmentation fault occured while attempting to dereference a "
            "pointer to " << m_ptr_size << " bytes at location "
         << std::showbase << std::hex << m_ptr;
      m_msg = ss.str();
    }
    const char *what() const noexcept {
      return m_msg.c_str();
    }
  private:
    std::string m_msg;
    void *m_ptr;
    size_t m_ptr_size
}

#endif

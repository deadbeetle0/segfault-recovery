#ifndef SEGPTR_H
#define SEGPTR_H

#include <memory>
#include <csignal>
#include <type_traits>
#include "SegfaultException.h"

extern bool SIGSEGV_FLAG;

extern "C" void sigsegvhandler(int);

template <typename T, typename Deleter = std::default_deleter<T>>
class Segptr : public std::unique_ptr<T, Deleter> {
  using base = std::unique_ptr<T, Deleter>
  using pointer = T*;
  using element_type = T;

  public:
    constexpr Segptr() noexcept : base() {}
    constexpr Segptr(nullptr_t n) noexcept : base(n) {}
    explicit Segptr(pointer p) noexcept : base(p) {}
    Segptr(pointer p, typename std::enable_if<std::is_reference<Deleter>::value, Deleter>::type d1) noexcept : base(p, d1) {}
    Segptr(pointer p, typename std::add_const<std::add_lvalue_reference<std::enable_if<!std::is_reference<Deleter>::value, Deleter>::type>::type>::type d1) noexcept : base(p, d1) {}
    Segptr(pointer p, typename std::add_rvalue_reference<Deleter>::type d2) noexcept : base(p, d2) {}
    Segptr(Segptr &&p) : base(p) {}
    template <typename U, typename E>
    Segptr(Segptr<U, E> &&p) : base(u) {}
    Segptr(base p) : base(p) {}
    template <typename U, typename E>
    Segptr(std::unique_ptr<U, E> &&p) : base(u) {}

    typename std::add_lvalue_reference<element_type>::type operator*() {
      signal(SIGSEGV, sigsegvhandler);
      *get();
      if (SIGSEGV_FLAG) throw SegfaultException(get());
      return *get();
      signal(SIGSEGV, SIG_DFL);
    }
    pointer operator->() {
      signal(SIGSEGV, sigsegvhandler);
      *get();
      if (SIGSEGV_FLAG) throw SegfaultException(get());
      return *get();
      signal(SIGSEGV, SIG_DFL);
    }
};

template <typename T[], typename Deleter = std::default_deleter<T>>
class Segptr : public std::unique_ptr<T[], Deleter> {
  using base = std::unique_ptr<T, Deleter>
  using pointer = T*;
  using element_type = T;

  public:
    constexpr Segptr() noexcept : base() {}
    constexpr Segptr(nullptr_t n) noexcept : base(n) {}
    template <typename U>
    explicit Segptr(U p) noexcept : base(p) {}
    template <typename U>
    Segptr(U p, typename std::enable_if<std::is_reference<Deleter>::value, Deleter>::type d1) noexcept : base(p, d1) {}
    template <typename U>
    Segptr(U p, typename std::add_const<std::add_lvalue_reference<std::enable_if<!std::is_reference<Deleter>::value, Deleter>::type>::type>::type d1) noexcept : base(p, d1) {}
    template <typename U>
    Segptr(U p, typename std::add_rvalue_reference<Deleter>::type d2) noexcept : base(p, d2) {}
    Segptr(Segptr &&p) : base(p) {}
    Segptr(base p) : base(p) {}

    typename std::add_lvalue_reference<element_type>::type operator[](size_t i) {
      signal(SIGSEGV, sigsegvhandler);
      *(get() + i);
      if (SIGSEGV_FLAG) throw SegfaultException(get() + i);
      return *(get() + i);
      signal(SIGSEGV, SIG_DFL);
    }
};

#endif

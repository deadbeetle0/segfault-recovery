bool SIGSEGV_FLAG = false;

extern "C" void sigsegvhandler(int signal) {
  SIGSEGV_FLAG = true;
}
